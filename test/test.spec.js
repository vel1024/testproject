/* global require describe it */

// 테스트를 위한 코드
const request = require("supertest");
const app = require("../app");

// root 주소에 대한 GET 테스트 코드
describe("GET /", () => {
	it("정상적인 요청, 200", (done) => {
		request(app)
			.get("/")
			.expect(200)
			.end((err, res) => { // eslint-disable-line no-unused-vars
				if (err) {
					throw err;
				}
				done();
			});
	});
});

// node 기반의 앱 에서 test 를 하기위해선 mocha 라이브러리를 써야한다
// mocha 라이브러리를 쓰기 위해선 mocha test/test.spec.js 라고 치거나
// 혹은 npm test script 를 mocha 라고 지정한뒤에 npm test 를 쓰면 된다
// 참조 : https://heropy.blog/2018/03/16/mocha/