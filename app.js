/* global require process module */ // eslint 에서 제외시키는 방법은 global 로 정의하던지 아니면 따로 옆에 다른 주석을 단다
const express = require("express");
const app = express();
const PORT = process.env.PORT || 3000;

app.get("/", (req, res) => res.send("Hello, World"));
app.get("/delivery", (req, res) => res.send("OK"));
app.get("/ping", (req, res) => res.send("pong"));
app.get("/error", (req, res) => res.status(500).send("Internal Server Error"));
app.get("/code", (req, res) => res.send("good"));

app.listen(PORT, () => console.log(`Test App Listening at http://localhost:${PORT}`));

module.exports = app;